# Various dotfiles for my [Void](https://voidlinux.org/) - [River](https://github.com/riverwm/river) setup.
River is a dynamic tiling Wayland compositor with flexible runtime configuration.

![ScreenShot](screenshot.jpg)

## Details

Below is a list of some of the packages that I use for my current setup which consists of two screens. A monitor as the main screen and the laptop screen as a slave and Kanshi is used to set the monitor as the left most and apply focus to it using a riverctl focus-output command. Please note that my config isn't perfect so your mileage may vary.<br />
The wallpaper featured in the screenshots can be found here: https://cdna.artstation.com/p/assets/images/images/015/554/914/large/artur-sadlos-to-sh300-ooh-as-05i.jpg
<br />
- **Operating System** --- [Void](https://voidlinux.org/)
- **Boot Loader** --- [Grub](https://www.gnu.org/software/grub/index.html)
- **Resource Monitor** --- [Btop](https://github.com/aristocratos/btop)
- **Window Manager** --- [River](https://github.com/riverwm/river)
- **Status Bar** --- [Yambar](https://codeberg.org/dnkl/yambar)
- **Screen Detection** --- [wlr-randr](https://sr.ht/~emersion/wlr-randr/)
- **Screen Hotplug** --- [Kanshi](https://sr.ht/~emersion/kanshi/)
- **Screen Locker** --- [Swaylock](https://github.com/swaywm/swaylock)
- **Screenshots** --- [Grim](https://sr.ht/~emersion/grim/)
                  --- [Slurp](https://github.com/emersion/slurp)
- **Idle Management Daemon** --- [Swayidle](https://github.com/swaywm/swayidle)
- **Shell** --- [Bash](https://www.gnu.org/software/bash/) using [Starship](https://starship.rs/) 
- **Completion** --- [Bash Completion](https://github.com/scop/bash-completion)
- **Terminal** --- [Foot](https://codeberg.org/dnkl/foot)
- **Notification Daemon** --- [Dunst](https://github.com/dunst-project/dunst)
- **Application Launcher** --- [Fuzzel](https://codeberg.org/dnkl/fuzzel)
- **File Manager** --- [Ranger](https://github.com/ranger/ranger)
- **Image Viewer** --- [Imv](https://git.sr.ht/~exec64/imv/)
- **Document Scanner** -- [Simple-scan](https://github.com/GNOME/simple-scan)
- **Editor** --- [Neovim](https://github.com/neovim/neovim)
  - **Plugins**
  	- [packer](https://github.com/wbthmason/packer.nvim)
	- [gruvbox](https://github.com/ellisonleao/gruvbox.nvim)
	- [nvim-tree.lua](https://github.com/kyazdani42/nvim-tree.lua)
	- [nvim-web-devicons](https://github.com/kyazdani42/nvim-web-devicons)
	- [mason.nvim](https://github.com/williamboman/mason.nvim)
	- [mason-lspconfig.nvim](https://github.com/williamboman/mason-lspconfig.nvim)
	- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig)
	- [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)
	- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
	- [nvim-colorizer.lua](https://github.com/norcalli/nvim-colorizer.lua)
	- [tagbar](https://github.com/preservim/tagbar)
	- [alpha-vim](https://github.com/goolord/alpha-vim)
	- [alpha-nvim-fortune](https://github.com/BlakeJC94/alpha-nvim-fortune)
	- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
	- [cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp)
	- [luasnip](https://github.com/L3MON4D3/Luasnip)
	- [cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua)
	- [cmp-buffer](https://github.com/hrsh7th/cmp-buffer)
	- [cmp-path](https://github.com/hrsh7th/cmp-path)
	- [cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline)
	- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)
	- [plenary.nvim](https://github.com/nvim-lua/plenary.nvim)
	- [indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim)
	- [undotree](https://github.com/jiaoshijie/undotree)
	- [Comment.nvim](https://github.com/numToStr/Comment.nvim)
	- [lsp_lines.nvim](https://git.sr.ht/~whynothugo/lsp_lines.nvim)
    - [bufdelete.nvim](https://github.com/famiu/bufdelete.nvim)
- **Web Browser** --- [Qutebrowser](https://www.qutebrowser.org)
- **Multimedia Framework**
	- **PipeWire**
		- [pipeWire](https://pipewire.org/)
		- [alsa-pipewire](https://pipewire.org/)
		- [libjack-pipewire](https://pipewire.org/)
		- [wireplumber](https://gitlab.freedesktop.org/pipewire/wireplumber/)
		- [libpulseaudio](https://www.freedesktop.org/wiki/Software/PulseAudio/)
- **PDF Viewer** --- [Zathura](https://git.pwmt.org/pwmt/zathura)
- **IRC** --- [Weechat](https://weechat.org/)
- **RSS Feed Reader** --- [Newsboat](https://newsboat.org/)
- **Youtube Downloader** --- [Yt-dlp (youtube-dl fork)](https://github.com/yt-dlp/yt-dlp)
- **Video player** --- [mpv](https://github.com/mpv-player/mpv)
- **Email Client** 
    - [aerc](https://git.sr.ht/~rjarry/aerc)
    - [isync](https://isync.sourceforge.io/)
    - [msmtp](https://marlam.de/msmtp/)
    - [dante](https://www.inet.no/dante/)
    - [w3m](https://w3m.sourceforge.net/)
    - [msmtpqueue](https://github.com/marlam/msmtp-mirror/tree/master/scripts/msmtpqueue)

## Keybindings
### Window manager controls
| Keys                                 | Action                    |
| ------------------------------------ | ------------------------- |
| <kbd>MOD + q</kbd>                   | Kill window               |
| <kbd>MOD+SHIFT + RETURN</kbd>        | Spawn terminal            |
| <kbd>MOD + d</kbd>                   | Fuzzel menu               |
| <kbd>CTRL + MOD + p</kbd>            | Fuzzel power menu         |

## To set the wallpaper

From within ranger if you highlight a valid image file and enter "wp" it will set that image as the default wallpaper.
